﻿using System;
using System.Threading;

namespace ConsoleGame
{
    struct Pos
    {
        public Pos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public int x, y;
    }
        
    internal class Program
    {
        private static int _platPos;
        private static int _prevPlatPos;
        private static int _platSize = 6;
        private static Pos _ballPos;
        private static Pos _prevBallPos;
        private static int _ballSpeed;
        private static int[] _ballDividers;
        private static int _xOffset;
        private static int _yOffset;
        private static int _ballIterCounter;
        private static int _scrHeight;
        private static int _scrWidth;

        public static void Main(string[] args)
        {
            Init();            
            DrawBox();
            DrawPlatform();
            SceneLoop();
            Console.Clear();
        }

        private static void Init()
        {
            _scrWidth = Console.WindowWidth;
            _scrHeight = Console.WindowHeight;
            Console.Clear();
            Console.CursorVisible = false;
            Console.Title = "Tennis game";
            // approximately set the platform pos to the center 
            _platPos = _scrWidth / 2 - _platSize / 2;
            _ballPos = new Pos(2, 2);
            _prevBallPos = _ballPos;
            _ballIterCounter = 1;
            _xOffset = _yOffset = 1;
            _ballSpeed = 0;
            _ballDividers = new [] {13, 11, 7, 5};
        }

        private static void DrawBox()
        {
            // draw top border
            for (int i = 0; i < _scrWidth; i++)
            {
                if (i == 0)
                    DrawSymAt(new Pos(i, 0),'┏');
                else if (i == _scrWidth - 1)
                    DrawSymAt(new Pos(i, 0),'┓');
                else
                    DrawSymAt(new Pos(i, 0),'━');
            }
            // draw left and right border
            for (int i = 1; i < _scrHeight - 1; i++)
            {
                DrawSymAt(new Pos(0, i), '┃'); // left
                DrawSymAt(new Pos(_scrWidth - 1, i), '┃'); // right
            }
        }
        
        // main loop 
        private static void SceneLoop()
        {
            _prevPlatPos = _platPos;
            if (Console.KeyAvailable)
            {
                switch (Console.ReadKey(false).Key)
                {
                        case ConsoleKey.Escape:
                            goto Exit;
                        case ConsoleKey.LeftArrow:
                            if(_platPos != 0)
                                --_platPos;
                            break;
                        case ConsoleKey.RightArrow:
                            if(_platPos + _platSize != _scrWidth)
                                ++_platPos;
                            break;
                }
            }
            // 
            if(_prevPlatPos != _platPos)
                DrawPlatform();
            // Exit if the ball won't bounce over the platform
            if(!DrawBall())
                goto Exit;
            // Permanently pressed key are buffered in MacOS 'iTerm' so the platform will
            // move even if the key already unpressed.
            // For MacOS 'iTerm' sleep for 30ms the most ideal value to solve the problem.
            // I dont know how the game will behave in Windows.
            Thread.Sleep(30);
            SceneLoop();
            Exit:;
        }

        private static void DrawPlatform()
        {
            // clear the previous
            for (int i = 0; i < _platSize; i++)
            {
                DrawSymAt(new Pos(_prevPlatPos+i, _scrHeight-1), ' ');
            }   
            // draw a new platform
            for (int i = 0; i < _platSize; i++)
            {
                DrawSymAt(new Pos(_platPos+i, _scrHeight-1), '-');
            }
        }

        private static bool IsBallBounced()
        {
            for (int i = 0; i < _platSize; i++)
            {
                if (_ballPos.x == _platPos + i)
                {
                    IncrSpeed();
                    return true;   
                }
            }
            return false;
        } 

        private static bool DrawBall()
        {
            // As 30ms is a really small gap, the ball will move ridiculosly fast.
            // So I've decided to use a frame divider for the ball redraw
            ++_ballIterCounter;
            if (_ballDividers[_ballSpeed] % _ballIterCounter == 0)
            {
                _ballIterCounter = 1;
                // crear the prev
                DrawSymAt(_prevBallPos, ' ');
                _prevBallPos = _ballPos;
                // draw a new
                DrawSymAt(_ballPos, '*');
                if (_ballPos.y == _scrHeight - 2)
                {
                    _yOffset -= 2;
                    if (!IsBallBounced())
                        return false;
                    Console.Beep(150, 200);
                }
                else if (_ballPos.y == 1)
                {
                    _yOffset += 2;
                    Console.Beep(150, 200);

                }
                if (_ballPos.x == _scrWidth - 2)
                {
                    _xOffset -= 2;
                    Console.Beep(150, 200);

                }
                else if (_ballPos.x == 1)
                {
                    _xOffset += 2;
                    Console.Beep(150, 200);

                }

                _ballPos.x += _xOffset;
                _ballPos.y += _yOffset;
            }
            return true;
        }
        // draw a symbol at a defined position
        private static void DrawSymAt(Pos p,char s)
        {
            Console.CursorLeft = p.x;
            Console.CursorTop = p.y;
            Console.Write(s);
        }

        private static void IncrSpeed()
        {
            if(_ballSpeed != _ballDividers.Length - 1)
                _ballSpeed++;
        }
    }
}